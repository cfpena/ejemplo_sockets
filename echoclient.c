#include "csapp.h"
#include <string.h>
int main(int argc, char **argv)
{
	int clientfd;
	char *port, *filename;
	char *host, buf[MAXLINE];
	rio_t rio;

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <filename>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	filename = argv[3];

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	
	strcpy(buf,filename);
	Rio_writen(clientfd, "start", strlen(buf));

	strcat(filename,"\n");

	Rio_writen(clientfd, filename, strlen(filename));

	while (1) {
		Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);
	}

/*
	strcpy(buf,filename);
	Rio_writen(clientfd, buf, strlen(buf));
	//Rio_readlineb(&rio, buf, MAXLINE);
	//Fputs(buf, stdout);
*/
	Close(clientfd);
	exit(0);
}
