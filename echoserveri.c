#include "csapp.h"
#include <sys/stat.h>
#include <string.h>
void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	struct stat file_stat;
	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		
		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;

	FILE *archivo;
	char nombreArchivo[MAXLINE];
	Rio_readinitb(&rio, connfd);
	struct stat file_stat;
	while(1) {
		Rio_readlineb(&rio, buf, sizeof(buf));
		strcpy(nombreArchivo,buf);
		printf("buscando archivo: %s", nombreArchivo);
		printf("asdfa");
		archivo = fopen(nombreArchivo, "r");
		if (archivo == NULL){
			printf("archivo no existe");
			Rio_writen(connfd, "archivo no existe", n);
		}else{
			sendfile( (int)connfd, archivo, NULL, file_stat.st_size );
			Rio_writen(connfd, "Enviado correctamente", n);
			printf("archivo si existe");
		}
		printf("%s", archivo);
		Rio_writen(connfd, buf, n);
	}
}
